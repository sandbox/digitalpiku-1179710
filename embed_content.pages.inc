<?php
// $Id$: embed_content.pages.inc,v 1.01 2011/06/08 19:58:48 digitalpiku Exp $

/**
 * @file
 * Page callbacks for the embed_content module.
 */

function embed_content_render() {
	
	global $base_url, $theme;
	$decoded = explode(":", base64_decode(arg(2),2));
	$view = arg(3);	
	
	if($view=='view') {
		
		$hook_name = $decoded[0] . '_block_view';		
		$block = $hook_name($decoded[1]);
		$theme_info = drupal_get_path('theme', $theme) . '/' . $theme . '.info';
		$info = file_get_contents($theme_info);
		
		$info_arr = explode("stylesheets[all][]", $info);
		$chunk = count($info_arr);
		$css_paths = array();
		
		for ($i=1;$i<$chunk;$i++) {
			$styles = explode("\n", $info_arr[$i]);
			$css_paths[$i] = trim(str_replace("=", "", $styles[0]));
		}
		
		$output = '';
		foreach ($css_paths as $css_path)
		$output .= '<link rel="stylesheet" href="' . $base_url . '/' . drupal_get_path('theme', $theme) . '/' . $css_path . '" >';
		$output .= '<link rel="stylesheet" href="' . $base_url . '/' . drupal_get_path('module', 'embed_content') . '/embed_content.css" >';		
		$output .= "<div id='content' class='content'><div class=\"demo front-page-block\">\n";
		$output .= "<div id=\"front-block-title\"><h2>" . $block['subject'] . "</h2></div>\n";
		$output .= "<div class=\"content\">" . $block['content'] . "</div>\n";
		$output .= "</div></div>\n";
		
		echo $output;
		exit();
	}else{
	
		drupal_add_http_header('Content-Type', 'text/javascript');
		echo 'document.write(\'<iframe style="border:none;" height="200" scrolling="no" name="iframe_embed_content" src="' . $base_url . '/'.'embed_content/render/'.arg(2).'/view"  ></iframe>\');';
		exit();
	}
	
	
	
		
}
 
function embed_content_get_code() {

	global $base_url;
	$module = arg(2);	
	$delta = arg(3);
	
	$jslink = base64_encode($module . ':' . $delta);	
	$hook_name = $module . '_block_view';
		
	$block = $hook_name($delta);
	
	$output = "<div class=\"demo front-page-block\">\n";
	$output .= "<div id=\"front-block-title\"><h2>" . $block['subject'] . "</h2></div>\n";
	$output .= "<div class=\"content\">" . $block['content'] . "</div>\n";
	$output .= "</div>\n";
	
	
	$form['block_view'] = array(
	'#type' => 'item',
	'#markup' => $output
	);
	
	
	$form['embed_txtarea'] = array(
	'#type' => 'textarea',
	'#title' => 'Embed Code for Block',
	'#rows' => 3,
	'#value' => '<script type="text/javascript" src="' . $base_url . '/embed_content/render/' . $jslink . '"></script>',
	'#description' => 'Copy this code and paste this in other website\'s content to display the above block dynamically. '	
	);
	
	return render($form);
}
